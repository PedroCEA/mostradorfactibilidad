<?php
	$conn = mysqli_connect("localhost", "root", "", "cea");
	error_reporting(0);
	error_reporting(E_ALL ^ E_NOTICE);
?>
<html>  
    <head>  
        <title>Detalle de Desarrolladores</title>  
          
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
          <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
          <link type="text/css" href="css/style.css" rel="stylesheet" />
          <link rel="stylesheet" type="text/css" href="css/miestilo.css">
     </head>     
    <body> 
    <nav class="navcea">
      <a class="navbar-brand" href="index.php">
        <img src="img/bizagi-logo.png" width="180" height="65" >
      </a>
  </nav> 
        <div class="container">
   <br />
   
   <h3 align="center" class="h3"><strong>Detalle de Alta Desarrollador</strong></a></h3><br />
   <br />
   
   <form method="post" id="user_form">
    <div class="table-responsive">
     <table class="table table-hover table-light" id="user_data">
      <thead>
        <tr>
          <th>No.</th> 
          <th>RFC</th>
          <th>Nombre o Razón Social</th>
          <th>Acción</th>
        </tr>
          
      </thead>
      <tbody>
      
      </tbody>
     </table> 
    </div>
   </form>

   <br />
  </div>
  <?php   
    require 'detalle.php';
   ?>
  <div id="action_alert" title="Action">

  </div>
 </body>
  <script src="https://code.jquery.com/jquery-3.5.1.js" ></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="js/detalleDesarrollador.js"></script>  
  <script src="js/main.js"></script> 
</html> 
<?php
mysqli_close($conn);
?>
