<?php

require_once 'db/conexion.php';
$columns = array('No','RFC','Nombre');

$query = "SELECT * FROM padrondesarrollador ";

 if (isset($_POST["search"]["value"])) {
 	$query .= 'where validado = 0 AND idPadron LIKE "%'.$_POST["search"]["value"].'%" 
 	           OR validado = 0 AND RFC LIKE "%'.$_POST["search"]["value"].'%"
 	           OR validado = 0 AND Nombre LIKE "%'.$_POST["search"]["value"].'%"';
 }

if(isset($_POST["order"]))
{
 $query .= 'ORDER BY '.$columns[$_POST['order']['0']['column']].' '.$_POST['order']['0']['dir'].'';
}
else
{
 $query .= 'ORDER BY idPadron DESC ';
}

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($conn, $query));

$result = mysqli_query($conn, $query .$query1);

$data = array();

while($row = mysqli_fetch_array($result))
{
 $sub_array = array();
 $sub_array[] = '<div class="update" data-id="'.$row["idPadron"].'" data-column="No">' . $row["idPadron"] . '</div>';
 $sub_array[] = '<div class="update" data-id="'.$row["idPadron"].'" data-column="RFC">' . $row["RFC"] . '</div>';
 $sub_array[] = '<div class="update" data-id="'.$row["idPadron"].'" data-column="Nombre">' . $row["Nombre"] . '</div>';
 $sub_array[] = '<button type="button" name="'.$row["idPadron"].'" class="detalle btn btn-primary" ><i class="far fa-eye"></i></button> 
 				 <button type="button" name="'.$row["idPadron"].'" class="eliminar btn btn-danger "><i class="far fa-trash-alt"></i></button>
 				 <button type="button" name="'.$row["idPadron"].'" class="correo btn btn-warning "><i class="far fa-envelope"></i></button>';
 $data[] = $sub_array;
}

function get_all_data($conn)
{
 $query = "SELECT * FROM padrondesarrollador where validado = 0";
 $result = mysqli_query($conn, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($conn),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

?>