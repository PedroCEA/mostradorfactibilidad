$('#btn_login_details').click(function(){
  
  var error_rfc='';
  var error_razon='';

  if($.trim($('#rfc').val()).length == 0 ){
    error_rfc = 'El rfc es requerido';
   $('#error_RFC').text(error_rfc);
   $('#rfc').css("border-color","red");
   $('#rfc').addClass('invalid');
  }else{
    error_rfc= '';
    $('#error_RFC').text(error_rfc);
    $('#rfc').css("border-color","#ced4da");
    $('#rfc').removeClass('invalid');
  }


  if($.trim($('#razon').val()).length == 0){
    error_razon = 'Escribe el nombre o razon social';
   $('#error_razon').text(error_razon);
   $('#razon').css("border-color","red");
   $('#razon').addClass('invalid');
  }else{
    error_razon= '';
    $('#error_razon').text(error_razon);
    $('#razon').css("border-color","#ced4da");
    $('#razon').removeClass('invalid');
  }

  if(error_rfc != '' || error_razon != '')
  {
    return false;
  }else{
    $('#list_login_details').removeClass('active active_tab1');
    $('#list_login_details').removeAttr('href data-toggle');
    $('#login_details').removeClass('active');
    $('#list_login_details').addClass('inactive_tab1');
    $('#list_personal_details').removeClass('inactive_tab1');
    $('#list_personal_details').addClass('active_tab1 active');
    $('#list_personal_details').attr('href', '#personal_details');
    $('#list_personal_details').attr('data-toggle', 'tab');
    $('#personal_details').removeClass('fade');
  }

 });


