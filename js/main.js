init();
var datatable;

function init(){
    getData();
}

function getData(){
     dataTable = $('#user_data').DataTable({
     "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            },
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"fetch.php",
     type:"POST"
    }
   });
}




$(document).on('click','.detalle',function(){
    var ver_id=$(this).attr('name');
    $.ajax({
        url:"DetalleDesarrollador.php",
        type:"post",
        data:{ver_id:ver_id},
        success:function(data){
            $("#info-detalle").html(data);
            $(".modal-header").css('background-color','#3393FF'); 
            $(".modal-header").css('color','white'); 
            $(".modal-title").text("Detalle del Desarrollador");
            $("#detalleData").modal('show');
        }
    });
});   

// function AlertaEliminacion(){
//     
   
//   }
// })
// }


$(document).on('click','.eliminar',function(){
    var delete_id=$(this).attr('name');
     Swal.fire({
            title: 'Estas seguro de borrar el registro?',
            text: "No se podra deshacer esta solicitud",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar!',
            cancelButtonText: "No",   
            allowOutsideClick: false
         }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url:"eliminar.php",
                        type:"post",
                        data:{delete_id:delete_id},
                        success:function(data){
                            $('#user_data').DataTable().destroy();
                            getData();
                            Swal.fire(
                                'Eliminado!',
                                'El registro fue eliminado.',
                                'success'
                            )
                        }
                    });
                }else{
                    console.log("nel");
                }
            
       })  
          
    }); 

$(document).on('click','.correo',function(){
    var correo_id=$(this).attr('name');
    $.ajax({
        url:"reenviarcorreo.php",
        type:"post",
        data:{correo_id:correo_id},
        success:function(data){
            //console.log(data);
             Swal.fire(
                'Se envio',
                'Su correo se mando correctamente',
                'success'
)
        }
    });
    

}); 