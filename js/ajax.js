$(document).ready(function(){
    tablaCasos=$("#tblCasos").DataTable({
        "columnDefs":[{
            "targets":-1,
            "data":null,
            "defaultContent":"<button type='button' class='editar btn btn-primary'><i class='fa fa-pencil'></i></button>"
        }],
        "language": {
    "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            }
    });

$("#btnNuevo").click(function(){
  $("#alta-caso").trigger("reset");
  //Creamos el caso
     var url = "creaCaso.php"; // El script a dónde se realizará la petición.
      var rfc = $('#rfcN').val();
      $.ajax({
        type: "POST",
        url:url,
        data: {"rfc":rfc},
        success: function(r)
            {
              console.log(r);
              $('#radNumber').val(r);
                
               
            },
    });
   $(".modal-header").css('background-color','#3393FF'); 
   $(".modal-header").css('color','white'); 
   $(".modal-title").text("Nueva Renovación de Autoabasto");
  
    $('#addModal').modal({ backdrop: 'static', keyboard: false });
    $('#addModal').modal('show');
});


 $(".editar").click(function(){
   //$("#alta-caso").trigger("reset");
   $(".modal-header").css('background-color','#3393FF'); 
   $(".modal-header").css('color','white'); 
   $(".modal-title").text(" Editar Renovación de Autoabasto");
    fila = $(this).closest("tr");
   
    id = parseInt(fila.find('td:eq(0)').text());
    radnumber = fila.find('td:eq(1)').text();
    
    var midVal = $('#numCaso').val(id);
    var midValRad = $('#numRadNumber').val(radnumber);
    
     obtenerForm();
    $("#editarModal").modal('show');
   
 })
});

 function obtenerForm(){
        selectRadNumber= $("#numRadNumber").val();
        selectIdCase=$("#numCaso").val();

     //alert(selectIdCase);
    $.ajax({
        type: "POST",
        url:"obtenerDatosEditar.php",
        data: {"selectRadNumber":selectRadNumber,"selectIdCase":selectIdCase},
        success: function(r)
            {
                $("#editarForm").html(r);
               
            },
    });
}


function realizaProceso(){
    var email = $("#inEmail").val();
    var expediente = $("#n_expediente").val();
    var factibilidad = $("#factibilidades_slc").val();
    var archivo1 = $("#filePicker").val();
    var archivo2 = $("#filePicker2").val();
    var nombre = $("#nombre").val();
    var telefono = $("#telefono").val();
    var correoREP = $("#correoREP").val();
    var SeleccionadoREP = $("#SeleccionadoREP").val();
    var ExisteexpedienteOR = $("#ExisteexpedienteOR").val();
    var reqdomestico = $("#reqdomestico").val();
    var reqcomercial = $("#reqcomercial").val();
    var reqindustrial = $("#reqindustrial").val();
    var reqespecificar = $("#reqespecificar").val();
    var reqmixto = $("#reqmixto").val();
    var totalunidades = $("#totalunidades").val();
   
    if (email == "") 
    {
        toastr.error("No ha ingresado correo","Aviso!");
        return false;    
    }
   
    if (expediente == "") {
        toastr.error("No ha ingresado un numero de expediente","Aviso!");
        return false;   
    }
    if (factibilidad == "") {
        toastr.error("No ha ingresado un numero de factibilidad","Aviso!");
        return false;   
    }
    if (archivo1 == "") {
        toastr.error("No has cargado el FORMATO DE SOLICITUD DEL DESARROLLADOR","Aviso!");
        return false;   
    }
    if (nombre == "") {
        toastr.error("No ha ingresado el nombre del Representante","Aviso!");
        return false;   
    }
    if (telefono == "") {
        toastr.error("No ha ingresado el telefono del Representante","Aviso!");
        return false;   
    }
    if (correoREP == "") {
        toastr.error("No ha ingresado el correo del Representante","Aviso!");
        return false;   
    }
	if (SeleccionadoREP== "") {
        toastr.error("No ha seleccionado el Representante","Aviso!");
        return false;   
    }
	if (ExisteexpedienteOR == "") {
        toastr.error("No ha Confirmado la factibilidad","Aviso!");
        return false;   
    }
	if (reqdomestico == "") {
        toastr.error("No ha ingresado el requerimiento domestico","Aviso!");
        return false;   
    }
	if (reqcomercial == "") {
        toastr.error("No ha ingresado el requerimiento comercial","Aviso!");
        return false;   
    }
	if (reqindustrial == "") {
		toastr.error("No ha ingresado el requerimiento industrial","Aviso!");
        return false;   
    }
	if (reqmixto == "") {
        toastr.error("No ha ingresado el requerimiento para el especifico","Aviso!");
        return false;   
    }
	if (totalunidades == "") {
        toastr.error("No ha ingresado el total de unidades","Aviso!");
        return false;   
    }
   var url = "llamarWebService.php"; // El script a dónde se realizará la petición.
      $.ajax({
        beforeSend: function(){
            $('#precarga').html('<span>Un momento por favor ...</span>');
        },
             type: "POST",
             url: url,
             data: $("#alta-caso").serialize(), // Adjuntar los campos del formulario enviado.
             success: function(data)
             {
                $('#precarga').hide(1000);
                
               // alert(data);
                //$("#respuesta").html(data); // Mostrar la respuestas del script PHP.
              
             },
             complete: function()
             {
                 var folio = $('#radNumber').val();
                swal({
                    title: 'Buen trabajo!',
                    text: "Tu trámite ha sido enviada con el folio:"+folio,
                    type: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK!'
                  }).then((result) => {
                    if (result.value) {
                      window.location.reload();
                    }
                  })
              
                
             }         
                //$("#respuesta").hide(3500);
           
            
             
           });
           //
           
        return false; // Evitar ejecutar el submit del formulario.
           
}

function realizaProcesoEditar(){
    var url = "llamarWebServiceEditar.php"; // El script a dónde se realizará la petición.
    var numCaso=$("#numCaso").val();
    var n_expediente=$("#n_expediente").val();
    var numRadNumber=$("#numRadNumber").val();
    var num_fac=$("#num_fac").val();
    var username=$("#edrfc").val();
    var numfact=$("#num_fac").val();
    var edfichero=$("#edfichero").val();
    var code1=$("#edbase64textarea").val();
    var edfichero2=$("#edfichero2").val();
    var code2=$("#edbase64textarea2").val();
    var nombreeditar=$("#nombreeditar").val();
    var telefonoeditar=$("#telefonoeditar").val();
    var correoREPeditar=$("#correoREPeditar").val();
    var SeleccionadoREPeditar=$("#SeleccionadoREPeditar").val();
    var Existeexpediente=$("#Existeexpediente").val();
    var edreqdomestico=$("#edreqdomestico").val();
    var edreqcomercial=$("#edreqcomercial").val();
    var edreqindustrial=$("#edreqindustrial").val();
    var edreqespecificar=$("#edreqespecificar").val();
    var edreqmixto=$("#edreqmixto").val();
    var edtotalunidades=$("#edtotalunidades").val();
	var rfc=$("#rfc").val();
    //alert(nombreeditar);
    var param={
                 'numCaso':numCaso,
                 'numRadNumber':numRadNumber,
                 'n_expediente':n_expediente,
                 'num_fac':num_fac,
                 'username':username,
                 'numfact':numfact,
                 'edfichero':edfichero,
                 'code1':code1,
                 'edfichero2':edfichero2,
				 'code2':code2,
                 'nombreeditar':nombreeditar,
                 'telefonoeditar':telefonoeditar,
                 'correoREPeditar':correoREPeditar,
                 'SeleccionadoREPeditar':SeleccionadoREPeditar,
				 'Existeexpediente':Existeexpediente,
				 'edreqdomestico':edreqdomestico,
				 'edreqcomercial':edreqcomercial,
				 'edreqindustrial':edreqindustrial,
				 'edreqespecificar':edreqespecificar,
				 'edreqmixto':edreqmixto,
				 'edtotalunidades':edtotalunidades,
				 'rfc':rfc
    }
   

      $.ajax({
        
        beforeSend: function(){
            $('#precarga').html('<img id="loader" src="../img/loading.gif" />');
        },

             type: "POST",
             url: url,
             data: param,
             dataType: "html", // Adjuntar los campos del formulario enviado.
             success: function(data)
             {
                
                $('#precarga').hide(1000);
                //$("#precarga").html(data); // Mostrar la respuestas del script PHP.
               // alert(data);

             },
             complete: function()
             {
                var folio = $('#numRadNumber').val();
                swal({
                    title: 'Buen trabajo!',
                    text: "Tu trámite ha sido enviada con el folio:"+folio,
                    type: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK!'
                  }).then((result) => {
                    if (result.value) {
                      window.location.reload();
                    }
                  })
              
               
             }         
                //$("#respuesta").hide(3500);
           
            
             
           });
           //
           
        return false; // Evitar ejecutar el submit del formulario.
           

}

function recargarLista(){
    $.ajax({
        type: "POST",
        url: "obtenerFactibilidad.php",
        data: $("#alta-caso").serialize(),
        success:function(r){
            $('#select2lista').html(r);
        }
    });
}    

function mostrarRelacion()
{
    var url = "mostrarDetalleFactibilidad.php"; // El script a dónde se realizará la petición.
    $.ajax({
           type: "POST",
           url: url,
           data: $("#alta-caso").serialize(), // Adjuntar los campos del formulario enviado.
           success: function(data)
           {
               $("#tabla").html(data); // Mostrar la respuestas del script PHP.
           },

           complete: function()
           {
             // $("#respuesta").hide(3500);
           }
          
         });
	$.ajax({
        type: "POST",
        url: "obtenerRequerimientos.php",
        data: $("#alta-caso").serialize(),
        success:function(r){
            $('#selectrequerimientos').html(r);
        }
    });
         
    return false; // Evitar ejecutar e
}

